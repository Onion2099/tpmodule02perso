﻿using System;

namespace TpModule02Perso
{
    public interface Forme
    {
        public void Aire();
        public void Perimetre();
        public void MessageCalcul();

    }
}
