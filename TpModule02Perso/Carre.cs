﻿using System;

namespace TpModule02Perso
{
    public class Carre : Forme
    {
        public int Longueur { get; set; }

        public void Aire()
        {
            double aire = Longueur * Longueur;
            System.Console.WriteLine($"Aire = {aire}");
        }

        public void Perimetre()
        {
            double perimetre = 4 * Longueur;
            System.Console.WriteLine($"Périmètre = {perimetre}");
        }
        public void MessageCalcul()
        {
            System.Console.WriteLine($"Carré de coté={Longueur}");
        }
    }
}
