﻿using System;

namespace TpModule02Perso
{
    public class Cercle : Forme
    {
        public int Rayon { get; set; }

        public void Aire()
        {
            double aire = Rayon * Rayon * Math.PI;
            System.Console.WriteLine($"Aire = {aire}");
        }

        public void Perimetre()
        {
            double perimetre = 2 * Math.PI * Rayon;
            System.Console.WriteLine($"Périmètre = {perimetre}");
        }

        public void MessageCalcul()
        {
            System.Console.WriteLine($"Cercle de rayon {Rayon}");
        }
    }
}
